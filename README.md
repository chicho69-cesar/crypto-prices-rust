# Crypt Prices App

Aplicacion para revisar el precio de cualquier criptomoneda programada en Rust

### Como utilizar este proyecto

Para empezar debes tener instalado: 
- Rust
- Cargo
- Editor de codigo

Despues debes de clonar este repositorio:
    
    git clone https://gitlab.com/chicho69-cesar/crypto-prices-rust.git

Por ultimo para compilar el proyecto solo utilice:

    cargo build

Para ejecutar el proyecto: 

    cargo run